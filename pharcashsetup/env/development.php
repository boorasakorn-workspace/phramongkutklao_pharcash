<?php
	defined('CHECKENV')  		OR define('CHECKENV', 'DEV');
	defined('SSLPREFIX')  		OR define('SSLPREFIX', 'http://');
	defined('TESTMODE')  		OR define('TESTMODE','Y');
	//Redirect Login
	defined('REDIRECTENABLED')  OR define('REDIRECTENABLED',false);
	defined('REDIRECTLOGIN')  	OR define('REDIRECTLOGIN','');
	defined('PHARPGCONNECT')	OR define('PHARPGCONNECT', array(
																'host' => '', 
																'user' => '', 
																'pass' => '', 
																'db' => ''
															)
														);
	defined('DASHBOARDPHARCASH')OR define('DASHBOARDPHARCASH','https://pmk.express-apps.com/pharcashapp');
	
	error_reporting(-1);
	ini_set('display_errors', 1);
?>