<?php
	defined('CHECKENV')  		OR define('CHECKENV', 'TEST');
	defined('SSLPREFIX')  		OR define('SSLPREFIX', 'https://');
	defined('HTTPHOST')			OR define('HTTPHOST','pmk.express-apps.com/pharcashsetup');
	defined('TESTMODE')  		OR define('TESTMODE','Y');
	//Redirect Login
	defined('REDIRECTENABLED')  OR define('REDIRECTENABLED',false);
	defined('REDIRECTLOGIN')  	OR define('REDIRECTLOGIN','');//defined('REDIRECTLOGIN')  	OR define('REDIRECTLOGIN','http://healthyflow.xyz/login');
	defined('PHARPGCONNECT')	OR define('PHARPGCONNECT', array(
																'host' => '', 
																'user' => '', 
																'pass' => '', 
																'db' => ''
															)
														);
	defined('DASHBOARDPHARCASH')OR define('DASHBOARDPHARCASH','https://pmk.express-apps.com/pharcashapp');
	
	ini_set('display_errors', 0);
	if (version_compare(PHP_VERSION, '5.3', '>='))
	{
		error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED & ~E_STRICT & ~E_USER_NOTICE & ~E_USER_DEPRECATED);
	}
	else
	{
		error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_USER_NOTICE);
	}
?>